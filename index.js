"use strict";
const dbjs = require("./db.js");
const files = require("./utils/file.js");
exports.handler = async (event) => {
  const db = await dbjs.get();

  const data = await db
    .collection("hotstar-testAnalytics")
    .findOne({ "uuid._id": event.params.path.testUUID }, { uuid: 1, info: 1 });

  let result = {
    version: "1.2",
    uuid: {
      _id: data.uuid._id,
      testId: data.uuid.testId,
      sessionId: data.uuid.sessionId,
      userDisplayName: data.uuid.userDisplayName,
      userEmail: data.uuid.userEmail,
      username: data.uuid.username,
    },
    files: files.list(data.info.testOutputArtifacts),
  };

  const response = {
    statusCode: 200,
    body: result,
    headers: {
      "Access-Control-Allow-Headers":
        "Content-Type, Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Access-Control-Allow-Origin",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT",
    },
  };
  return response;
};
