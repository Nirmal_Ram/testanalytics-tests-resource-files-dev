"use strict";
const filename = require("./getFileName.js");
module.exports.list = (object) => {
  let result = [];
  Object.entries(object).forEach((data) => {
    if (data[0] == "deviceScreenRecord") {
      result.push({
        url: data[1].url[0],
        fileName: filename.fetch(data[1].url[0]),
      });
    } else if (data[0] == "deviceScreenshots") {
      let response = [];
      data[1].url.map((itm) => {
        response.push({
          url: itm,
          fileName: filename.fetch(itm),
        });
      });
      result.push(response);
    } else {
      result.push({
        url: data[1].url,
        fileName: filename.fetch(data[1].url),
      });
    }
  });
  return result;
};
