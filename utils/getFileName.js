"use strict";
module.exports.fetch = (url) => {
    return String(url).split(/[#?]/)[0].split('/').pop().trim();
  };